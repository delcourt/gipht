import sys, signal

from PySide2.QtWidgets import QApplication
from src.MainWindow import MainWindow
from src.GuiController import GuiController

if __name__ == "__main__":
    print("\t\t\t#########################################################")
    print("\t\t\t##################   WELCOME TO GIPHT   #################")
    print("\t\t\t         BEFORE YOU START PLEASE READ THE MANUAL:")
    print("\t\t\thttps://gitlab.cern.ch/smaier/ph2_ot_module_test_manual")
    print("\t\t\t#########################################################")
    
    app = QApplication(sys.argv)
    signal.signal(signal.SIGINT,signal.SIG_DFL)
    window = MainWindow( )
    controller = GuiController( window )
    controller.start()
    window.show()
    window.setWindowTitle("Graphical user Interface for PHase 2 Tracker objects - GIPHT v4")
    sys.exit(app.exec_())