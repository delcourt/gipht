from PySide2.QtCore import Signal, QObject
import json, os, subprocess, stat, yaml
from datetime import datetime



class DB_Interface(QObject):

    update = Signal ( object , object )

    def __init__( self, pDev =False,p2FA=False ):
        super(DB_Interface, self).__init__(  )
        self.query_cmd = "python3 py4dbupload/run/query.py "
        self.pipeIndex = 0
        self.development = pDev
        self.twoFA = p2FA
        self.locations = []
        self.testConditions = {"PS":[], "2S": [], "Skeleton": []}
        self.busy = False
        #if not stat.S_ISFIFO(os.stat(self.pipe).st_mode) or not os.path.isfile(self.pipe):

    def getLocations( self ):
        cmd = self.query_cmd + " --locations"
        pipe = self.run_command(cmd)
        self.locations = self.read_data(pipe, False)
        return self.locations

    def getModuleInformation( self, pModule ):
        cmd = self.query_cmd + " --otModuleInformation="+pModule
        pipe = self.run_command(cmd)
        return self.read_data(pipe, True)

    def getTestConditions( self ):
        cmd = self.query_cmd + " --testConditions2s"
        pipe = self.run_command(cmd)
        conditions_2s = self.read_data(pipe, False)
        #Get most recent list element (highest version number)
        self.testConditions["2S"] = sorted(conditions_2s, key=lambda d: d.get('version', 0)) [-1]

        cmd = self.query_cmd + " --testConditionsPs"
        pipe = self.run_command(cmd)
        conditions_Ps = self.read_data(pipe, False)

        self.testConditions["PS"] = sorted(conditions_Ps, key=lambda d: d.get('version', 0)) [-1]

        cmd = self.query_cmd + " --testConditionsSkeleton"
        pipe = self.run_command(cmd)
        conditions_Skeleton = self.read_data(pipe, False)
        self.testConditions["Skeleton"] = sorted(conditions_Skeleton, key=lambda d: d.get('version', 0)) [-1]

        return self.testConditions

    def run_command( self, pCommand, pPipe = True):
        self.busy = True
        return_code = 400

        cmd = 'source py4dbupload/bin/setup.sh && ' + pCommand
        if self.development:
            cmd += " --dev "
        if self.twoFA:
            cmd += " --2fa "
        if pPipe:
            #Return data is read from the pipe
            self.pipeIndex = self.pipeIndex + 1
            #Avoid cross talk. Each query has its own pipe
            pipe = '/tmp/db_pipe_' + str(self.pipeIndex)
            try:
                os.mkfifo(pipe)
            except FileExistsError:
                pass
            cmd += " -p "+ pipe
            subprocess.Popen(cmd , executable="/bin/bash", shell=True)
            return pipe
        else:
            #Capture return code from the buffer
            print(cmd)
            process = subprocess.Popen(cmd, executable="/bin/bash", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, err = process.communicate()
            for line in str.splitlines(stdout.decode()):
                print(line)
                if "Response code" in line:
                    return_code = int(line.split(":")[1])
            
            return return_code

    def read_data( self, pPipe, pPrint= False ):
        str_data = ''
        with open(pPipe, "r") as pipe:
            str_data = pipe.read()
            #while True:
            #    line = pipe.readline()
            #    if len(line) == 0:
            #        break
            #    else:
            #        str_data += line
        data = json.loads(str_data)
        if pPrint:
            [print(datapoint) for datapoint in data]
        self.busy = False
        return data

    def createModuleIVCSV( self, pRun, pDirectory, pDataFile ):

        db_file = "IV_DB_CSV_FILE.csv"
        file = pDirectory + pDataFile["FileName"]
        targetFile = pDirectory + db_file
        pDataFile["DBFile"] = db_file   

        with open(file, 'r') as f:
            fileContent = yaml.safe_load(f)

        mean_temp = None
        mean_rh = None

        if "Temperature" in fileContent["Data"][0].keys():
            mean_temp = 0
            mean_rh = 0

            for dataPoint in fileContent["Data"]:
                mean_temp += float(dataPoint["Temperature"])
                mean_rh += float(dataPoint["Relative Humidity"])

            mean_temp = mean_temp / len(fileContent["Data"])
            mean_rh = mean_rh / len(fileContent["Data"])

        f = open(targetFile, 'w')

        date = datetime.strptime(pRun.runInfo["Date"],'%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')

        f.write('#NameLabel,'   + pRun.runInfo["Module_ID"] + "\n")
        f.write('#Date,'        + date   + "\n")
        f.write("#Comment,"     + pDataFile["Comment"] + "\n")
        f.write("#Location,"    + pRun.runInfo["Location"]+ "\n")
        f.write("#Inserter,"    + pRun.runInfo["Operator"]+ "\n")
        f.write("#RunType,"     + pRun.runInfo["Run_type"]+ "\n")
        f.write("\n")
        f.write("STATION, AV_TEMP_DEGC, AV_RH_PRCNT"+ "\n")
        if mean_temp is not None:
            f.write(pRun.runInfo["Station_Name"] + "," + str(mean_temp) + "," + str(mean_rh) + "\n")
        else:
            f.write(pRun.runInfo["Station_Name"] + "\n")

        f.write("\n")
        f.write("VOLTS, CURRNT_NAMP, TEMP_DEGC, RH_PRCNT, TIME\n")
        for dataPoint in fileContent["Data"]:
            #ts = datetime.strptime(dataPoint["Timestamp"],'%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S') 
            f.write(str(dataPoint["Voltage"])       + "," + 
                    str(dataPoint["Current"])       + "," + 
                    str(dataPoint["Temperature"])   + "," + 
                    str(dataPoint["Relative Humidity"]) + "," +
                    str(dataPoint["Timestamp"]) + "\n")

        f.close()

     #self.db.createModuleTestCSV(run, runItem.summaryFile, dataFile["FileName"] ,directory + db_file)
    def createModuleTestCSV( self, pRun, pSummaryFile, pDirectory, pDataFile ):

        db_file = "ModuleTest_DB_CSV_FILE.csv"
        targetFile = pDirectory + db_file
        rootFile = pDataFile["FileName"]
        pDataFile["DBFile"] = db_file   

        rh_mean = None
        t_mean = None
        with open(pDirectory + pSummaryFile, 'r') as f:
            fileContent = yaml.safe_load(f)
            rh_mean = sum([float(dataPoint["Humidity"]) for dataPoint in fileContent])/len(fileContent)
            t_mean  = sum([float(dataPoint["Temperature"]) for dataPoint in fileContent])/len(fileContent)



        f = open(targetFile, 'w')

        date = datetime.strptime(pRun.runInfo["Date"],'%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')

        f.write('#NameLabel,'   + pRun.runInfo["Module_ID"] + "\n")
        f.write('#Date,'        + date   + "\n")
        f.write("#Comment,"     + pDataFile["Comment"] + "\n")
        f.write("#Location,"    + pRun.runInfo["Location"]+ "\n")
        f.write("#Inserter,"    + pRun.runInfo["Operator"]+ "\n")
        f.write("#RunType,"     + pRun.runInfo["Run_type"]+ "\n")
        f.write("\n")

        f.write("ROOT_FILE, ROOT_DATE, ROOT_VER, ROOT_CONF_VER, ROOT_TEMP_DEGC"+ "\n")
        f.write(rootFile + "," +  date + ", v1-00, v1-00,{:.2f}".format(t_mean))

        f.close()     

    def uploadFile( self, pFile, pRootFile = "" ):
        cmd = ""
        if "IV" in pFile:
            cmd = "python3 ./py4dbupload/run/uploadOTModuleIV.py --upload --store --data=" + pFile
            #cmd = "python3 ../py4DBupload/run/uploadOTModuleIV.py --store --data=" + pFile
        elif "ModuleTest" in pFile:
            cmd = "python3 ./py4dbupload/run/uploadOTModuleTestRootFile.py --upload --store --data=" + pFile
            #cmd = "python3 ../py4DBupload/run/uploadOTModuleTestRootFile.py --store --data=" + pFile

        return self.run_command(cmd, False)
"""
{'Date': '20-02-2024 16:09:18', 'LocalRunNumber': 313, 'Location': 'KIT', 'Operator': 'Stefan Maier', 'Result_Folder': 'Results/ModuleTest_313/'}
{'Data': [{'Current': -2323.39, 'Timestamp': '2024-02-20 16:09:51', 'Voltage': 0.028334}, {'Current': -2671.1499999999996, 'Timestamp': '2024-02-20 16:09:55', 'Voltage': -9.967285}, {'Current': -2808.92, 'Timestamp': '2024-02-20 16:09:58', 'Voltage': -19.965469}, {'Current': -2857.27, 'Timestamp': '2024-02-20 16:10:02', 'Voltage': -29.9751}],
'Info': {'ID': '2S_18_5_KIT-00001'},
'Summary': {}}
"""
#IV FILE
"""
#NameLabel,2S_18_5_FNL-00002
#Date,2024-01-24 15:53:00
#Comment,abc
#Location,Fermilab
#Inserter,Stefan Maier
#RunType,mod_final

STATION, AV_TEMP-DEGC, AV_RH_PRCNT
KIT-ModuleReadout-Station1, 20, 5

VOLTS, CURRNT_NAMP, TEMP_DEGC, RH_PRCNT, TIME
1.0,0.1,17,5,2024-01-24 15:53:00
2.0,0.2,18,6,2024-01-24 15:54:00
3.0,0.3,19,7,2024-01-24 15:55:00
4.0,0.4,20,8,2024-01-24 15:56:00
5.0,0.5,21,9,2024-01-24 15:57:00
6.0,0.6,22,8,2024-01-24 15:58:00
7.0,0.7,23,7,2024-01-24 15:59:00
8.0,0.8,24,6,2024-01-24 16:00:00
9.0,0.9,25,5,2024-01-24 16:01:00
"""

"""
#NameLabel,2S_18_5_FNL-00002
#Date,2024-01-24 15:53:00
#Comment,abc
#Location,Fermilab
#Inserter,Stefan Maier
#RunType,mod_final

ROOT_FILE, ROOT_DATE, ROOT_VER, ROOT_CONF_VER, ROOT_TEMP_DEGC,
ROOT_OTModuleTestRootFile.root, 2024-02-10 01:48:54, v1-00, v1-00, 20
"""