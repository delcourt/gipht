#------------------------------------------------
#+++++++++++++PipeReader.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      17.08.2020
#------------------------------------------------


from PySide2.QtCore import QObject, Signal, QRunnable
import re

regexp = re.compile("([a-z]+):(.*)")

class Message:
    def __init__(self, opcode, data):
        self.opcode = opcode
        self.data = data

class PipeSignal(QObject):
    update = Signal ( object, object )

class PipeReader(QRunnable):
    def __init__( self ,pProcess, pPipe ):
        super(PipeReader, self).__init__()
        self.process = pProcess
        self.pipe = pPipe
        self.signal = PipeSignal()
        print("GIPHT:\tPipeReader for " + self.pipe)
        #the kill state is checked in the while True loop to abort the readout
        self.kill = False
        #If there is not data available this flag is raised
        self.dataAvailable = False

    #Starts the killing procedure by setting kill = True
    def killYourself( self ):
        print("GIPHT:\tPipeReader for " + self.pipe + " going to be killed by hijacking the pipe and inserting an empty line")
        f = open(self.pipe, "w")
        f.write("\n")
        f.close()
        self.kill = True

    #Called by the while run() loop, if called once, data is available
    #Emits all signals to the corresponging objects ( FC7Board or PowerSupply )
    def decodePipeMessageAndEmitSignal(self, pMessage):
        r = regexp.search(pMessage)
        if r != None:
            r1 = r.group(1)
            r2 = r.group(2)
        else:
            r1 = "info"
            r2 = "-"
        message = Message(r1, r2)


        if message.opcode in ["info","warn","err","msg","stat"]:
            self.signal.update.emit( message.opcode, message.data )
        elif message.opcode in ["prog"]:
            progress = float(message.data)
            self.signal.update.emit( message.opcode, progress * 100 )
        elif message.opcode in ["data"]:
            self.signal.update.emit( message.opcode, message.data )
            
    #Working method of the pipereader. Reads the named pipe in a while loops, is stopped,
    #when the pipe is empty of the kill flag is set.
    def run( self ):
        with open (self.pipe) as p: #Only works when the pipe is opened from the other side, if not GIPHT STUCKS
            while True:
                line = p.readline()
                if len(line) == 0 or self.kill == True:
                    self.signal.update.emit( "finished" , None )
                    if not self.dataAvailable:
                        self.signal.update.emit( "noData" , None )
                    break
                #print(line)
                else:
                    self.dataAvailable = True
                self.decodePipeMessageAndEmitSignal(line)
        print("GIPHT:\tPipeReader " + self.pipe + " finished run() loop")
