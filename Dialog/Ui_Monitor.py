# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Monitor.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Monitor(object):
    def setupUi(self, Monitor):
        if not Monitor.objectName():
            Monitor.setObjectName(u"Monitor")
        Monitor.resize(1322, 755)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Monitor.sizePolicy().hasHeightForWidth())
        Monitor.setSizePolicy(sizePolicy)
        Monitor.setMinimumSize(QSize(0, 40))
        self.monitorTabs = QTabWidget(Monitor)
        self.monitorTabs.setObjectName(u"monitorTabs")
        self.monitorTabs.setGeometry(QRect(4, 9, 1311, 731))
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayoutWidget = QWidget(self.tab)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(9, 19, 1281, 651))
        self.lowVoltageGraphLayout = QGridLayout(self.gridLayoutWidget)
        self.lowVoltageGraphLayout.setObjectName(u"lowVoltageGraphLayout")
        self.lowVoltageGraphLayout.setContentsMargins(0, 0, 0, 0)
        self.monitorTabs.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.gridLayoutWidget_3 = QWidget(self.tab_2)
        self.gridLayoutWidget_3.setObjectName(u"gridLayoutWidget_3")
        self.gridLayoutWidget_3.setGeometry(QRect(10, 20, 1281, 651))
        self.highVoltageGraphLayout = QGridLayout(self.gridLayoutWidget_3)
        self.highVoltageGraphLayout.setObjectName(u"highVoltageGraphLayout")
        self.highVoltageGraphLayout.setContentsMargins(0, 0, 0, 0)
        self.monitorTabs.addTab(self.tab_2, "")
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.gridLayoutWidget_2 = QWidget(self.tab_3)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(10, 20, 1281, 651))
        self.tRhGraphLayout = QGridLayout(self.gridLayoutWidget_2)
        self.tRhGraphLayout.setObjectName(u"tRhGraphLayout")
        self.tRhGraphLayout.setContentsMargins(0, 0, 0, 0)
        self.monitorTabs.addTab(self.tab_3, "")
        self.tab_4 = QWidget()
        self.tab_4.setObjectName(u"tab_4")
        self.onlyShowSlotChannelsCheckBox = QCheckBox(self.tab_4)
        self.onlyShowSlotChannelsCheckBox.setObjectName(u"onlyShowSlotChannelsCheckBox")
        self.onlyShowSlotChannelsCheckBox.setEnabled(False)
        self.onlyShowSlotChannelsCheckBox.setGeometry(QRect(20, 20, 201, 25))
        self.monitorTabs.addTab(self.tab_4, "")

        self.retranslateUi(Monitor)

        self.monitorTabs.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Monitor)
    # setupUi

    def retranslateUi(self, Monitor):
        Monitor.setWindowTitle(QCoreApplication.translate("Monitor", u"Form", None))
        self.monitorTabs.setTabText(self.monitorTabs.indexOf(self.tab), QCoreApplication.translate("Monitor", u"Low Voltage", None))
        self.monitorTabs.setTabText(self.monitorTabs.indexOf(self.tab_2), QCoreApplication.translate("Monitor", u"High Voltage", None))
        self.monitorTabs.setTabText(self.monitorTabs.indexOf(self.tab_3), QCoreApplication.translate("Monitor", u"Temperature / Humidity", None))
        self.onlyShowSlotChannelsCheckBox.setText(QCoreApplication.translate("Monitor", u"Only show slot channels", None))
        self.monitorTabs.setTabText(self.monitorTabs.indexOf(self.tab_4), QCoreApplication.translate("Monitor", u"Settings", None))
    # retranslateUi

