# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ArduinoWidget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_ArduinoWidget(object):
    def setupUi(self, ArduinoWidget):
        if not ArduinoWidget.objectName():
            ArduinoWidget.setObjectName(u"ArduinoWidget")
        ArduinoWidget.resize(557, 749)
        self.gridLayoutWidget_2 = QWidget(ArduinoWidget)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(9, 9, 541, 721))
        self.gridLayout = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.saveButton = QPushButton(self.gridLayoutWidget_2)
        self.saveButton.setObjectName(u"saveButton")

        self.gridLayout.addWidget(self.saveButton, 1, 2, 1, 1)

        self.checkButton = QPushButton(self.gridLayoutWidget_2)
        self.checkButton.setObjectName(u"checkButton")

        self.gridLayout.addWidget(self.checkButton, 1, 1, 1, 1)

        self.configureLedsAndTriggerButton = QPushButton(self.gridLayoutWidget_2)
        self.configureLedsAndTriggerButton.setObjectName(u"configureLedsAndTriggerButton")

        self.gridLayout.addWidget(self.configureLedsAndTriggerButton, 4, 1, 1, 2)

        self.uploadArduinoSWButton = QPushButton(self.gridLayoutWidget_2)
        self.uploadArduinoSWButton.setObjectName(u"uploadArduinoSWButton")

        self.gridLayout.addWidget(self.uploadArduinoSWButton, 4, 0, 1, 1)

        self.version = QLabel(self.gridLayoutWidget_2)
        self.version.setObjectName(u"version")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.version.sizePolicy().hasHeightForWidth())
        self.version.setSizePolicy(sizePolicy)
        self.version.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.version, 0, 0, 1, 1)

        self.connectionLayout = QVBoxLayout()
        self.connectionLayout.setObjectName(u"connectionLayout")

        self.gridLayout.addLayout(self.connectionLayout, 5, 0, 1, 3)

        self.launchServerCheckBox = QCheckBox(self.gridLayoutWidget_2)
        self.launchServerCheckBox.setObjectName(u"launchServerCheckBox")

        self.gridLayout.addWidget(self.launchServerCheckBox, 3, 0, 1, 1)

        self.index = QLabel(self.gridLayoutWidget_2)
        self.index.setObjectName(u"index")
        sizePolicy.setHeightForWidth(self.index.sizePolicy().hasHeightForWidth())
        self.index.setSizePolicy(sizePolicy)
        self.index.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.index, 0, 1, 1, 1)

        self.arduinoChannelTreeWidget = QTreeWidget(self.gridLayoutWidget_2)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.arduinoChannelTreeWidget.setHeaderItem(__qtreewidgetitem)
        self.arduinoChannelTreeWidget.setObjectName(u"arduinoChannelTreeWidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.arduinoChannelTreeWidget.sizePolicy().hasHeightForWidth())
        self.arduinoChannelTreeWidget.setSizePolicy(sizePolicy1)
        self.arduinoChannelTreeWidget.setMinimumSize(QSize(0, 200))

        self.gridLayout.addWidget(self.arduinoChannelTreeWidget, 6, 0, 1, 3)

        self.indexLabel = QLabel(self.gridLayoutWidget_2)
        self.indexLabel.setObjectName(u"indexLabel")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.indexLabel.sizePolicy().hasHeightForWidth())
        self.indexLabel.setSizePolicy(sizePolicy2)

        self.gridLayout.addWidget(self.indexLabel, 0, 2, 1, 1)

        self.idLabel = QLabel(self.gridLayoutWidget_2)
        self.idLabel.setObjectName(u"idLabel")
        sizePolicy.setHeightForWidth(self.idLabel.sizePolicy().hasHeightForWidth())
        self.idLabel.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.idLabel, 2, 0, 1, 1)

        self.closeButton = QPushButton(self.gridLayoutWidget_2)
        self.closeButton.setObjectName(u"closeButton")

        self.gridLayout.addWidget(self.closeButton, 1, 0, 1, 1)

        self.idLineEdit = QLineEdit(self.gridLayoutWidget_2)
        self.idLineEdit.setObjectName(u"idLineEdit")

        self.gridLayout.addWidget(self.idLineEdit, 2, 1, 1, 2)

        self.externalServerIPPortLineEdit = QLineEdit(self.gridLayoutWidget_2)
        self.externalServerIPPortLineEdit.setObjectName(u"externalServerIPPortLineEdit")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.externalServerIPPortLineEdit.sizePolicy().hasHeightForWidth())
        self.externalServerIPPortLineEdit.setSizePolicy(sizePolicy3)
        self.externalServerIPPortLineEdit.setMinimumSize(QSize(200, 0))
        self.externalServerIPPortLineEdit.setMaximumSize(QSize(200, 16777215))

        self.gridLayout.addWidget(self.externalServerIPPortLineEdit, 3, 2, 1, 1)

        self.externalServerIPPortLabel = QLabel(self.gridLayoutWidget_2)
        self.externalServerIPPortLabel.setObjectName(u"externalServerIPPortLabel")
        sizePolicy.setHeightForWidth(self.externalServerIPPortLabel.sizePolicy().hasHeightForWidth())
        self.externalServerIPPortLabel.setSizePolicy(sizePolicy)
        self.externalServerIPPortLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.externalServerIPPortLabel, 3, 1, 1, 1)


        self.retranslateUi(ArduinoWidget)

        QMetaObject.connectSlotsByName(ArduinoWidget)
    # setupUi

    def retranslateUi(self, ArduinoWidget):
        ArduinoWidget.setWindowTitle(QCoreApplication.translate("ArduinoWidget", u"Form", None))
        self.saveButton.setText(QCoreApplication.translate("ArduinoWidget", u"Save", None))
        self.checkButton.setText(QCoreApplication.translate("ArduinoWidget", u"Check", None))
        self.configureLedsAndTriggerButton.setText(QCoreApplication.translate("ArduinoWidget", u"Configure LEDs and Trigger", None))
        self.uploadArduinoSWButton.setText(QCoreApplication.translate("ArduinoWidget", u"Upload Arduino SW", None))
        self.version.setText(QCoreApplication.translate("ArduinoWidget", u"SW Version: ", None))
        self.launchServerCheckBox.setText(QCoreApplication.translate("ArduinoWidget", u"Launch Server", None))
        self.index.setText(QCoreApplication.translate("ArduinoWidget", u"Index", None))
        self.indexLabel.setText(QCoreApplication.translate("ArduinoWidget", u"IndexLabel", None))
        self.idLabel.setText(QCoreApplication.translate("ArduinoWidget", u"ID", None))
        self.closeButton.setText(QCoreApplication.translate("ArduinoWidget", u"Close", None))
        self.externalServerIPPortLineEdit.setText("")
        self.externalServerIPPortLabel.setText(QCoreApplication.translate("ArduinoWidget", u"External server IP:Port", None))
    # retranslateUi

