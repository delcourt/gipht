from dbwrapper.dbwrapper import DBWrapper
import os
# EXECUTE OUTSIDE OF dbwrapper FOLDER!!!!
if __name__ == "__main__":
    base_url    = "https://cmsomsdet.cern.ch/tracker-resthub/query"
    upload_url  = "https://cmsdca.cern.ch/trk_loader/trker/int2r"  #Test Data base
    #upload_url  = "https://cmsdca.cern.ch/trk_loader/trker/cmsr"

    theWrapper = DBWrapper( base_url, upload_url )
    success = False
    if os.path.exists('.session.cache'):
        success = True
    else:
        print("ask")
        success = theWrapper.askToStoreDBCredentials()
    if success:
        print("check")
        print("DB Connection: " + str(theWrapper.check_database()))
    else:
        print("-")
        #exit(0)
    #theWrapper.generateSensorDicingPlot()
    #with open("dbwrapper/DatabaseTemplates/dataExamples.yaml", 'r') as f:
    #    testData = yaml.safe_load(f)["moduleIvDemonstator"]
    #print(testData)
    #theWrapper.print2SModuleTable()
    #theWrapper.printPSModuleTable()

    #data = theWrapper.getMetrolgyResults() 
    #if data:
    #    theWrapper.printMetrologySummary( data )

    #print(theWrapper.getModuleInformation("2S_18_5_KIT-0001"))
    #theWrapper.getIvCurvesOfModule("2S_18_6_KIT-00008")
    #theWrapper.testUpload()
    #theWrapper.generateSensorDicingPlot()
    #theWrapper.test()
    #theWrapper.demonstrate_module_iv_upload_with_run_number(testData)






    #print(theWrapper.getPartInformation("2S_18_5_KIT-00001"))       #Get some information 

    #responseCode = self.uploadFile(moduleIvFile)
    #print(self.translateResponseCode(responseCode))
    #print(CRED + "No run number, no upload!" + CEND)
    #print("_____________________")

    #print(theWrapper.getRunNumber())
    #theWrapper.testUpload(True)
